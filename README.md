# autolabel bot

Simple usage:

    mv -v config.example.json config.json
    $EDITOR config.json
    python3 ./autolabel.py

`autolabel.py` will process all changes to all merge requests in selected projects since the last run.
It will, according to the comments, change the labels of corresponding MRs. Users should write in the comments
something like `please add "Needs Changes"` or `remove needs review`. More specifically, write some string
that matches `trigger_regex` in `autolabel.py`. The match is case-insensitive.

The last run timestamp is saved in `cache.json`. Remove it to make `autolabel.py` process all changes no matter
when they are made.

## Configuration

Configuration file is `config.json`. `token` is the [personal access token](https://invent.kde.org/help/user/profile/personal_access_tokens.md)
you get from GitLab website. `projects` is a list of the ids of projects where you would like 
the labels updated automatically. `labels` is a list of the names of labels that all users would
be able to add/remove. Only these labels will be added to or removed from the MRs.
`conflicting_labels` is a list of lists of names of labels that will conflict with
each other. (At most one of them can appear).

# Copyright info

Copyright (C) 2019 Tusooa Zhu <tusooa@vista.aero>

This file is part of autolabel-bot.

autolabel-bot is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

autolabel-bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with autolabel-bot.  If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python3
'''
    Copyright (C) 2019 Tusooa Zhu <tusooa@vista.aero>

    This file is part of autolabel-bot.

    autolabel-bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    autolabel-bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with autolabel-bot.  If not, see <https://www.gnu.org/licenses/>.
'''

import urllib.request
import urllib.parse
import json
import asyncio
import datetime
import re


SERVER_ADDR = 'https://invent.kde.org/api/v4'
token = None
last_updated = None
projects = []
aware_label_names = []
conflicting_labels = []
now = datetime.datetime.now()

def get(api):
    urllib.request()

with open('config.json', 'r') as file:
    data = json.load(file)
    token = data['token']
    projects = data['projects']
    aware_label_names = data['labels']
    conflicting_labels = data['conflicting_labels']

aware_label_restr = '|'.join(aware_label_names)
trigger_regex = re.compile(r'\b(?P<kw>change|add|remove)\b.*?(?P<label>\b{labels}\b)'
                           .format(labels=aware_label_restr),
                           flags=re.I)
remove_keywords_regex = re.compile(r'remove', flags=re.I)

try:
    with open('cache.json', 'r') as file:
        last_updated = datetime.datetime.utcfromtimestamp(json.load(file)['last_update_timestamp'])
except Exception:
    pass

def get_new_labels_from_notes(proj, mr, data):
    iid = mr['iid']

    if last_updated is not None:
        # XXX: will GitLab return the time string without .ffff?
        unread_notes = [note for note in data if datetime.datetime.strptime(note['updated_at'], '%Y-%m-%dT%H:%M:%S.%fZ') >= last_updated]
    else:
        unread_notes = data

    labels = set(mr['labels'])

    for note in unread_notes:
        body = note['body']

        for match in trigger_regex.finditer(body):
            keyword = match.group('kw')
            label = match.group('label')
            formal_label_name = [l for l in aware_label_names if label.lower() == l.lower()][0]

            if remove_keywords_regex.match(keyword):
                labels.discard(formal_label_name)
            else:
                labels.add(formal_label_name)
                for conflict_group in conflicting_labels:
                    if formal_label_name in conflict_group:
                        labels_to_remove = [l for l in conflict_group if formal_label_name != l]
                        for l in labels_to_remove:
                            labels.discard(l)

    return labels

async def fetch_mr(proj, mr):
    iid = mr['iid']
    print('Fetching {proj}!{iid}'.format(proj=proj, iid=iid))
    query = {
        'private_token': token,
        'sort': 'asc',
        'order_by': 'updated_at',
    }

    request = urllib.request.Request(SERVER_ADDR +
                                     '/projects/{id}/merge_requests/{iid}/notes?'.format(id=proj, iid=iid) +
                                     urllib.parse.urlencode(query))

    response = urllib.request.urlopen(request)

    if response.getcode() >= 400:
        print('Error loading MR {iid} for project #{proj_id}: {code} {reason}'
              .format(iid=iid, proj_id=proj, code=response.status, reason=response.reason))
        return False

    body = response.read().decode('utf-8')
    data = json.loads(body)
    new_labels = get_new_labels_from_notes(proj, mr, data)

    if new_labels == set(mr['labels']):
        # no changes needed
        return True

    print("Merge Request {id}!{iid}'s labels will be changed to {labels}"
          .format(id=proj, iid=iid, labels=new_labels))

    query = {
        'private_token': token,
        'labels': ','.join(new_labels),
    }

    request = urllib.request.Request(SERVER_ADDR +
                                     '/projects/{id}/merge_requests/{iid}'.format(id=proj, iid=iid),
                                     data=bytes(urllib.parse.urlencode(query), 'utf-8'),
                                     method='PUT')

    response = urllib.request.urlopen(request)

    if response.getcode() >= 400:
        print('Error changing MR {iid} for project #{proj_id}: {code} {reason}'
              .format(iid=iid, proj_id=proj, code=response.status, reason=response.reason))
        return False

    return True

async def fetch_project(proj):
    query = {
        'private_token': token,
        'state': 'opened',
    }

    if last_updated is not None:
        query['updated_after'] = last_updated

    request = urllib.request.Request(SERVER_ADDR +
                                     '/projects/{proj_id}/merge_requests?'.format(proj_id=proj) +
                                     urllib.parse.urlencode(query))

    response = urllib.request.urlopen(request)

    if response.getcode() >= 400:
        print('Error loading MRs for project #{proj_id}: {code} {reason}'
              .format(proj_id=proj, code=response.status, reason=response.reason))
        return False

    body = response.read().decode('utf-8')
    data = json.loads(body)

    if data:
        await asyncio.wait([fetch_mr(proj, mr) for mr in data])
    return True

async def main():
    await asyncio.wait([fetch_project(proj) for proj in projects])


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()

with open('cache.json', 'w') as file:
    data = {
        'last_update_timestamp': now.timestamp(),
    }
    json.dump(data, file)
